# Aleurone (Urine samples)

This is all the data processing and statistics that were done for the urine samples from the Aleurone study.
The paper is available here: [link when available]


## Using this repository

You will need to install [Git Large File Storage (LFS)](https://git-lfs.github.com) to be able download this repository.
To be sure that the scripts are reproducible you will need to use R version 3.2.2.
This repository is a so-called Packrat project. That means that it contains all used R packages and will thus stay reproducible.
In addition there are some packages that, on Windows, are practically impossible to install from source (which is what Packrat does). If/when it fails to install you can install them manually:

```
install.packages("git2r")
install.packages("jsonlite")
install.packages("RCurl")
install.packages("XML")
install.packages("mzR")
```

You will then have to manually copy the compiled packages from either "C:\Program Files\R\R-3.2.2\library" (or similar) or "C:\Users\[username]\Documents\R\win-library\3.2" to the "Aleurone\packrat\lib\x86_64-w64-mingw32\3.2.2" folder.


## Raw data

The raw data can be downloaded from MetaboLights ([link when available]). The raw data should be placed in folders NEG and POS respectively.


## Running the scripts

The file `00_flow.R` simply executes the other scripts.
`01_remove_orbitrap_shoulder_peak.R` removes so called shoulder peaks (orbitrap artifact) in the raw data and writes filtered files to the folders `POS_fixed` and `NEG_fixed`, respectively.
The remaining scripts are hopefully self-explanatory.

In the `Output` folder of the repository only selected output has been included due to size. The scripts can re-generate everything done in the analysis.
