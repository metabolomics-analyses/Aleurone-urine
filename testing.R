analyze.xcms.group(xset_g_r_g_fill,205.0970,3.49*60,
                   rt_tol_sample=60,
                   mz_tol_sample=0.01,
                   rt_tol_group=30,
                   mz_tol_group=0.05)




188.0704

3.48221


temp <- getPeaklist(xsaC)
                    
                    
select <- abs(temp$rt-3.48221*60)<5 & abs(temp$mz-205.0967)<0.01
temp[temp$pcgroup[select]==temp$pcgroup,c("rt","mz","pcgroup")] %>% arrange(mz)




select1 <- abs(temp$rt-3.48221*60)<5 & abs(temp$mz-205.0967)<0.01
select2 <- abs(temp$rt-3.48221*60)<5 & abs(temp$mz-188.0704)<0.01


dat1 <- temp %>% .[select1,,drop=FALSE] %>% select(starts_with("x")) %>% as.numeric
dat2 <- temp %>% .[select2,,drop=FALSE] %>% select(starts_with("x")) %>% as.numeric

plot(dat1,dat2)
cor(dat1,dat2)

